package academy.learnprogramming.rivu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ClickPostActivity extends AppCompatActivity
{
    private VideoView PostImage;


    private ProgressDialog loadingBar;
    private TextView PostDescription,MovieName;
    private Button EditPostButton , DeletePostButton;

    private FirebaseAuth mAuth;
    private DatabaseReference ClickPostRef;


    private String PostKey,currentUserID, databaseUserID, description,moviename,image;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_post);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();

        PostKey = getIntent().getExtras().get("PostKey").toString();
        ClickPostRef = FirebaseDatabase.getInstance().getReference().child("Posts").child(PostKey);

        PostImage = (VideoView) findViewById(R.id.click_post_image);
        final MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(PostImage);
        loadingBar = new ProgressDialog(this);

        PostDescription = (TextView) findViewById(R.id.click_post_description);
        MovieName = (TextView) findViewById(R.id.click_post_moviename);
        DeletePostButton = (Button) findViewById(R.id.delete_post_button);
        EditPostButton = (Button) findViewById(R.id.edit_post_button);

        DeletePostButton.setVisibility(View.INVISIBLE);
        EditPostButton.setVisibility(View.INVISIBLE);


        ClickPostRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.exists())
                {
                    description = dataSnapshot.child("description").getValue(String.class);
                    moviename = dataSnapshot.child("moviename").getValue(String.class);

                    image = dataSnapshot.child("postimage").getValue(String.class);
                    databaseUserID = dataSnapshot.child("uid").getValue(String.class);
                    MovieName.setText(moviename);
                    PostDescription.setText(description);
                    //Picasso.get().load(image).into(PostImage);
                    Uri uri = Uri.parse(dataSnapshot.child("postimage").getValue(true).toString());
                    PostImage.setVideoURI(uri);
                    PostImage.setMediaController(mediaController);
                    PostImage.requestFocus();
                    PostImage.start();

                    if(currentUserID.equals(databaseUserID))
                    {
                        DeletePostButton.setVisibility(View.VISIBLE);
                        EditPostButton.setVisibility(View.VISIBLE);
                    }
                    EditPostButton.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            EditCurrentPost(description);
                        }
                    });

                    DeletePostButton.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            DeleteCurrentPost();


                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    private void EditCurrentPost(String description)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(ClickPostActivity.this);
        builder.setTitle("Edit Post:");

        final EditText inputField = new EditText(ClickPostActivity.this);
        inputField.setText(description);
        builder.setView(inputField);

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                ClickPostRef.child("description").setValue(inputField.getText().toString());
                Toast.makeText(ClickPostActivity.this, "Post Updated Successfully...", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.cancel();
            }
        });

        Dialog dialogInterface = builder.create();
        dialogInterface.show();
        dialogInterface.getWindow().setBackgroundDrawableResource(android.R.color.holo_blue_light);
    }


    private void DeleteCurrentPost()
    {



            ClickPostRef.removeValue();
        loadingBar.setTitle("Deleting Post");
        loadingBar.setMessage("Please wait, while we are deleting your post...");
        loadingBar.show();
        loadingBar.setCanceledOnTouchOutside(true);
            SendUserToMainActivity();

    }


    private void SendUserToMainActivity()
    {
        Intent mainIntent = new Intent(ClickPostActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }



}
